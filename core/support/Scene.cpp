#include "core/support/Scene.h"
#include "gui/MdiSubWindowEdit.h"

Scene::Scene(MdiSubWindowEdit *mdiSubWindow_) : QGraphicsScene(mdiSubWindow_)
{
	m_parentWindow = mdiSubWindow_;
	setSceneRect(-10000,-10000,20000,20000);
}

void Scene::mouseReleaseEvent(QGraphicsSceneMouseEvent *e)
{
	QGraphicsScene::mouseReleaseEvent(e);
}

Scene::~Scene()
{
}
