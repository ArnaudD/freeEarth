#ifndef PLANET_H_INCLUDED
#define PLANET_H_INCLUDED

class Planet
{
	enum class Era {geologic, evolutionary, civilized, technological}; 
	
	public:
	Planet(Era);
	void stepForward();
	
	private:
		
	Era m_Era;
	int year;
};

#endif
