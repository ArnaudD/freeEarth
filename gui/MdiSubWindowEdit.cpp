#include "gui/MdiSubWindowEdit.h"
#include "gui/Window.h"

MdiSubWindowEdit::MdiSubWindowEdit(Window* parent_) : QMdiSubWindow(parent_)
{
	m_parent = parent_;	
	m_scene = new Scene(this);
	m_view = new View(m_scene, this);
	resize(600,450);
	
	// Get the title bar height and frame with to place correctly the buttons
	QStyle *style_ = style();
	int titleBarHeight = style_->pixelMetric(QStyle::PM_TitleBarHeight, 0, this);
	int frameWidth = style_->pixelMetric(QStyle::PM_MdiSubWindowFrameWidth, 0, this);
	
	// Left-hand buttons
	int iconSize = QPixmap("resources/sprites/SimEarthPC/windows/icons/editPlaceLife.png").width();
	int buttonSize = iconSize+8;
	int heightTextButton = 25;
	
	// Left-hand bar
	QWidget *leftBarWidget = new QWidget(this);
	leftBarWidget->move(frameWidth, titleBarHeight);
	leftBarWidget->setMaximumWidth(buttonSize*2);
	leftBarWidget->setMinimumWidth(buttonSize*2);
	
	// Layout
	delete layout();
	QHBoxLayout *layout = new QHBoxLayout;
    layout->addWidget(leftBarWidget, Qt::AnchorTop);
    layout->addWidget(m_view, Qt::AnchorTop);
	setLayout(layout);
	
	m_buttonPlaceLife = new QPushButton(QIcon("resources/sprites/SimEarthPC/windows/icons/editPlaceLife.png"), "", leftBarWidget);
	m_buttonPlaceLife->setStatusTip("Place Life");
	m_buttonPlaceLife->setFixedSize(buttonSize,buttonSize);
	m_buttonPlaceLife->setIconSize(QSize(iconSize,iconSize));
	m_buttonPlaceLife->move(0, 0);
	m_menuPlaceLife = new QMenu(this);
	m_buttonPlaceLife->setMenu(m_menuPlaceLife);
		
		m_actionPlaceProkaryote = m_menuPlaceLife->addAction("Prokaryote");
		m_actionPlaceProkaryote->setIcon(QIcon("resources/sprites/SimEarthPC/tiles/life/prokaryote/0.png"));
		
		m_actionPlaceEukaryote = m_menuPlaceLife->addAction("Eukaryote");
		m_actionPlaceEukaryote->setIcon(QIcon("resources/sprites/SimEarthPC/tiles/life/eukaryote/0.png"));
	
		m_actionPlaceRadiate = m_menuPlaceLife->addAction("Radiate");
		m_actionPlaceRadiate->setIcon(QIcon("resources/sprites/SimEarthPC/tiles/life/radiate/0.png"));
		
		m_actionPlaceArthropod = m_menuPlaceLife->addAction("Arthropod");
		m_actionPlaceArthropod->setIcon(QIcon("resources/sprites/SimEarthPC/tiles/life/arthropod/0.png"));
		
		m_actionPlaceMollusk = m_menuPlaceLife->addAction("Mollusk");
		m_actionPlaceMollusk->setIcon(QIcon("resources/sprites/SimEarthPC/tiles/life/mollusk/0.png"));
	
		m_actionPlaceFish = m_menuPlaceLife->addAction("Fish");
		m_actionPlaceFish->setIcon(QIcon("resources/sprites/SimEarthPC/tiles/life/fish/0.png"));
		
		m_actionPlaceCetacean = m_menuPlaceLife->addAction("Cetacean");
		m_actionPlaceCetacean->setIcon(QIcon("resources/sprites/SimEarthPC/tiles/life/cetacean/0.png"));
		
		m_actionPlaceTrichordate = m_menuPlaceLife->addAction("Trichordate");
		m_actionPlaceTrichordate->setIcon(QIcon("resources/sprites/SimEarthPC/tiles/life/trichordate/0.png"));
		
		m_actionPlaceInsect = m_menuPlaceLife->addAction("Insect");
		m_actionPlaceInsect->setIcon(QIcon("resources/sprites/SimEarthPC/tiles/life/insect/0.png"));
		
		m_actionPlaceAmphibian = m_menuPlaceLife->addAction("Amphibian");
		m_actionPlaceAmphibian->setIcon(QIcon("resources/sprites/SimEarthPC/tiles/life/amphibian/0.png"));
		
		m_actionPlaceReptile = m_menuPlaceLife->addAction("Reptile");
		m_actionPlaceReptile->setIcon(QIcon("resources/sprites/SimEarthPC/tiles/life/reptile/0.png"));
		
		m_actionPlaceDinosaur = m_menuPlaceLife->addAction("Dinosaur");
		m_actionPlaceDinosaur->setIcon(QIcon("resources/sprites/SimEarthPC/tiles/life/dinosaur/0.png"));
		
		m_actionPlaceAvian = m_menuPlaceLife->addAction("Avian");
		m_actionPlaceAvian->setIcon(QIcon("resources/sprites/SimEarthPC/tiles/life/avian/0.png"));

		m_actionPlaceMammal = m_menuPlaceLife->addAction("Mammal");
		m_actionPlaceMammal->setIcon(QIcon("resources/sprites/SimEarthPC/tiles/life/mammal/0.png"));
		
	m_buttonAltitude = new QPushButton(QIcon("resources/sprites/SimEarthPC/windows/icons/editSetAltitude.png"), "", leftBarWidget);
	m_buttonAltitude->setStatusTip("Set Altitude");
	m_buttonAltitude->setFixedSize(buttonSize,buttonSize);
	m_buttonAltitude->setIconSize(QSize(iconSize,iconSize));
	m_buttonAltitude->move(buttonSize-1, 0);
	
	m_buttonTriggerEvents = new QPushButton(QIcon("resources/sprites/SimEarthPC/windows/icons/editTriggerEvents.png"), "", leftBarWidget);
	m_buttonTriggerEvents->setStatusTip("Trigger Events");
	m_buttonTriggerEvents->setFixedSize(buttonSize,buttonSize);
	m_buttonTriggerEvents->setIconSize(QSize(iconSize,iconSize));
	m_buttonTriggerEvents->move(0, buttonSize-1);
	
	m_buttonMove = new QPushButton(QIcon("resources/sprites/SimEarthPC/windows/icons/editMove.png"), "", leftBarWidget);
	m_buttonMove->setStatusTip("Move");
	m_buttonMove->setFixedSize(buttonSize,buttonSize);
	m_buttonMove->setIconSize(QSize(iconSize,iconSize));
	m_buttonMove->move(buttonSize-1, buttonSize-1);
	
	m_buttonPlaceBiomes = new QPushButton(QIcon("resources/sprites/SimEarthPC/windows/icons/editPlaceBiome.png"), "", leftBarWidget);
	m_buttonPlaceBiomes->setStatusTip("Place Biome");
	m_buttonPlaceBiomes->setFixedSize(buttonSize,buttonSize);
	m_buttonPlaceBiomes->setIconSize(QSize(iconSize,iconSize));
	m_buttonPlaceBiomes->move(0, 2*buttonSize-1);
	m_menuPlaceBiomes = new QMenu(this);
	m_buttonPlaceBiomes->setMenu(m_menuPlaceBiomes);
	
		m_actionPlaceArctic = m_menuPlaceBiomes->addAction("Arctic");
		m_actionPlaceArctic->setIcon(QIcon("resources/sprites/SimEarthPC/tiles/biomes/arctic.png"));
	
		m_actionPlaceBoreal = m_menuPlaceBiomes->addAction("Boreal");
		m_actionPlaceBoreal->setIcon(QIcon("resources/sprites/SimEarthPC/tiles/biomes/boreal.png"));
		
		m_actionPlaceDesert = m_menuPlaceBiomes->addAction("Desert");
		m_actionPlaceDesert->setIcon(QIcon("resources/sprites/SimEarthPC/tiles/biomes/desert.png"));
		
		m_actionPlaceGrassland = m_menuPlaceBiomes->addAction("Grassland");
		m_actionPlaceGrassland->setIcon(QIcon("resources/sprites/SimEarthPC/tiles/biomes/grassland.png"));
		
		m_actionPlaceForest = m_menuPlaceBiomes->addAction("Forest");
		m_actionPlaceForest->setIcon(QIcon("resources/sprites/SimEarthPC/tiles/biomes/forest.png"));
	
		m_actionPlaceJungle = m_menuPlaceBiomes->addAction("Jungle");
		m_actionPlaceJungle->setIcon(QIcon("resources/sprites/SimEarthPC/tiles/biomes/jungle.png"));
		
		m_actionPlaceSwamp = m_menuPlaceBiomes->addAction("Swamp");
		m_actionPlaceSwamp->setIcon(QIcon("resources/sprites/SimEarthPC/tiles/biomes/swamp.png"));
	
	m_buttonExamine = new QPushButton(QIcon("resources/sprites/SimEarthPC/windows/icons/editExamine.png"), "", leftBarWidget);
	m_buttonExamine->setStatusTip("Examine");
	m_buttonExamine->setFixedSize(buttonSize,buttonSize);
	m_buttonExamine->setIconSize(QSize(iconSize,iconSize));
	m_buttonExamine->move(buttonSize-1, 2*buttonSize-1);
	
	// Active tool
	m_activeTool = new QLabel(leftBarWidget);
	m_activeTool->move(0, 3*buttonSize-1);
	m_activeTool->setFixedSize(buttonSize*2,buttonSize*2);
	QPalette pal = palette();
	pal.setColor(QPalette::Background, Qt::white);
	m_activeTool->setAutoFillBackground(true);
	m_activeTool->setPalette(pal);
	
	// Vizualisation buttons
	m_buttonToggleOceans = new QPushButton("Oceans", leftBarWidget);
	m_buttonToggleOceans->setCheckable(true);
	m_buttonToggleOceans->setChecked(true);
	m_buttonToggleOceans->move(0, 5*buttonSize-1);
	m_buttonToggleOceans->setFixedSize(buttonSize*2,heightTextButton);
	
	m_buttonToggleBiomes = new QPushButton("Biomes", leftBarWidget);
	m_buttonToggleBiomes->setCheckable(true);
	m_buttonToggleBiomes->setChecked(true);
	m_buttonToggleBiomes->move(0, 5*buttonSize-1+heightTextButton);
	m_buttonToggleBiomes->setFixedSize(buttonSize*2,heightTextButton);
	
	m_buttonToggleCities = new QPushButton("Cities", leftBarWidget);
	m_buttonToggleCities->setCheckable(true);
	m_buttonToggleCities->setChecked(true);
	m_buttonToggleCities->move(0, 5*buttonSize-1+2*heightTextButton);
	m_buttonToggleCities->setFixedSize(buttonSize*2,heightTextButton);
	
	m_buttonToggleLife = new QPushButton("Life", leftBarWidget);
	m_buttonToggleLife->setCheckable(true);
	m_buttonToggleLife->setChecked(true);
	m_buttonToggleLife->move(0, 5*buttonSize-1+3*heightTextButton);
	m_buttonToggleLife->setFixedSize(buttonSize*2,heightTextButton);
	
	m_buttonToggleEvents = new QPushButton("Events", leftBarWidget);
	m_buttonToggleEvents->setCheckable(true);
	m_buttonToggleEvents->setChecked(true);
	m_buttonToggleEvents->move(0, 5*buttonSize-1+4*heightTextButton);
	m_buttonToggleEvents->setFixedSize(buttonSize*2,heightTextButton);
	
	m_buttonToggleHeat = new QPushButton("Heat", leftBarWidget);
	m_buttonToggleHeat->setCheckable(true);
	m_buttonToggleHeat->move(0, 5*buttonSize+5*heightTextButton+10);
	m_buttonToggleHeat->setFixedSize(buttonSize*2,heightTextButton);
	
	m_buttonToggleRainfalls = new QPushButton("Rainfalls", leftBarWidget);
	m_buttonToggleRainfalls->setCheckable(true);
	m_buttonToggleRainfalls->move(0, 5*buttonSize+6*heightTextButton+10);
	m_buttonToggleRainfalls->setFixedSize(buttonSize*2,heightTextButton);
	
	m_buttonToggleWinds = new QPushButton("Winds", leftBarWidget);
	m_buttonToggleWinds->setCheckable(true);
	m_buttonToggleWinds->move(0, 5*buttonSize+7*heightTextButton+10);
	m_buttonToggleWinds->setFixedSize(buttonSize*2,heightTextButton);
	
	m_buttonToggleSeaFlow = new QPushButton("Sea Flow", leftBarWidget);
	m_buttonToggleSeaFlow->setCheckable(true);
	m_buttonToggleSeaFlow->move(0, 5*buttonSize+8*heightTextButton+10);
	m_buttonToggleSeaFlow->setFixedSize(buttonSize*2,heightTextButton);
	
	m_buttonToggleSeaHeat = new QPushButton("Sea Heat", leftBarWidget);
	m_buttonToggleSeaHeat->setCheckable(true);
	m_buttonToggleSeaHeat->move(0, 5*buttonSize+9*heightTextButton+10);
	m_buttonToggleSeaHeat->setFixedSize(buttonSize*2,heightTextButton);
	
	m_buttonToggleMagma = new QPushButton("Magma", leftBarWidget);
	m_buttonToggleMagma->setCheckable(true);
	m_buttonToggleMagma->move(0, 5*buttonSize+10*heightTextButton+10);
	m_buttonToggleMagma->setFixedSize(buttonSize*2,heightTextButton);
}



void MdiSubWindowEdit::closeEvent(QCloseEvent*  closeEvent_)
{
	hide();
//	closeEvent_->accept();
}
