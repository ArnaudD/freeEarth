#ifndef MDISUBWINDOWEDIT_H_INCLUDED
#define MDISUBWINDOWEDIT_H_INCLUDED

#include <QtWidgets>

#include "gui/View.h"
#include "core/support/Scene.h"

class Window;

class MdiSubWindowEdit: public QMdiSubWindow
{
	Q_OBJECT

	public:
		MdiSubWindowEdit(Window*);
		QPushButton *m_buttonPlaceLife;
		QPushButton *m_buttonAltitude;
		QPushButton *m_buttonTriggerEvents;
		QPushButton *m_buttonMove;
		QPushButton *m_buttonPlaceBiomes;
		QPushButton *m_buttonExamine;
		
		QMenu *m_menuPlaceLife;
		QAction *m_actionPlaceProkaryote;
		QAction *m_actionPlaceEukaryote;
		QAction *m_actionPlaceRadiate;
		QAction *m_actionPlaceArthropod;
		QAction *m_actionPlaceMollusk;
		QAction *m_actionPlaceFish;
		QAction *m_actionPlaceCetacean;
		QAction *m_actionPlaceTrichordate;
		QAction *m_actionPlaceInsect;
		QAction *m_actionPlaceAmphibian;
		QAction *m_actionPlaceReptile;
		QAction *m_actionPlaceDinosaur;
		QAction *m_actionPlaceAvian;
		QAction *m_actionPlaceMammal;
		QAction *m_actionPlaceStoneAge;
		QAction *m_actionPlaceBronzeAge;
		QAction *m_actionPlaceIronAge;
		QAction *m_actionPlaceIndustrialAge;
		QAction *m_actionPlaceAtomicAge;
		QAction *m_actionPlaceInfoAge;
		QAction *m_actionPlaceNanotechAge;
		QAction *m_actionPlaceBiomeFactory;
		QAction *m_actionPlaceOxygenator;
		QAction *m_actionPlaceN2Generator;
		QAction *m_actionPlaceVaporator;
		QAction *m_actionPlaceCO2Generator;
		QAction *m_actionPlaceMonolyth;
		QAction *m_actionPlaceIceMeteor;
		
		QMenu *m_menuPlaceBiomes;
		QAction *m_actionPlaceArctic;
		QAction *m_actionPlaceBoreal;
		QAction *m_actionPlaceDesert;
		QAction *m_actionPlaceGrassland;
		QAction *m_actionPlaceForest;
		QAction *m_actionPlaceJungle;
		QAction *m_actionPlaceSwamp;
		
		QLabel *m_activeTool;
		
		QPushButton *m_buttonToggleOceans;
		QPushButton *m_buttonToggleBiomes;
		QPushButton *m_buttonToggleCities;
		QPushButton *m_buttonToggleLife;
		QPushButton *m_buttonToggleEvents;
		QPushButton *m_buttonToggleHeat;
		QPushButton *m_buttonToggleRainfalls;
		QPushButton *m_buttonToggleWinds;
		QPushButton *m_buttonToggleSeaFlow;
		QPushButton *m_buttonToggleSeaHeat;
		QPushButton *m_buttonToggleMagma;
		
	protected:
		void closeEvent(QCloseEvent*);

	private:
		Window* m_parent;
		View* m_view;
		Scene* m_scene;
		
		// Status bar
		QStatusBar* m_statusBar;
		QLabel *m_labelX, *m_labelY;
};
#endif

